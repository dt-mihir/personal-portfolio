export const Projects = [
  {
    id: 1,
    slug: "listwebinars",
    title: "ListWebinars",
    brief:
      "ListWebinars provides a curated list of webinars happening across the world.",
    tags: ["#web"],
    tools: ["Typescript"],
    link: "foo.com",
  },
  {
    id: 2,
    slug: "avantika-alumni",
    title: "Avantika Alumni",
    brief:
      "Avantika Alumni aims at creating an ecosystem of network within alumnus and with activities happening at Avantika University.",
    tags: ["#android"],
    tools: ["Android", "Java", "PHP"],
    link: "foo.com",
  },
  {
    id: 3,
    slug: "smart-grid",
    title: "Smart Grid",
    brief:
      "Use of renewable energy resources to generate electricity to use as an alternative power supply.",
    tags: ["#internetofthings"],
    tools: ["Node.js", "Blynk"],
    link: "foo.com",
  },
  {
    id: 3,
    slug: "smart-grid",
    title: "Smart Grid",
    brief:
      "Use of renewable energy resources to generate electricity to use as an alternative power supply.",
    tags: ["#internetofthings"],
    tools: ["Node.js", "Blynk"],
    link: "foo.com",
  },
];

export const SkillList = [
  "React.js",
  "Next.js",
  "Node.js",
  "Express.js",
  "Typescript",
  "Vanilla Javascript",
  "PHP",
  "Styled-Components",
  "SCSS",
  "MongoDB",
  "MySQL",
  "jQuery",
];

export const Updates = [
  {
    id: 1,
    title: "Defining the role of community for ListWebinars",
    dateCreated: "2020-09-10",
    link: "foo.com",
  },
  {
    id: 2,
    title: "Learning about Go to develop performance-driven REST APIs",
    dateCreated: "2020-09-30",
    link: "foo.com",
  },
  {
    id: 3,
    title: "Got my PPO with Slang Labs",
    dateCreated: "2020-10-11",
    link: "foo.com",
  },
];

export const Tools = [
  {
    name: "Typescript",
    icon: "/images/icons8-typescript.svg",
  },
  {
    name: "Javascript",
    icon: "/images/icons8-javascript.svg",
  },
  {
    name: "Java",
    icon: "/images/icons8-java.svg",
  },
  {
    name: "Android",
    icon: "/images/icons8-android-os.svg",
  },
  {
    name: "PHP",
    icon: "/images/icons8-php-logo.svg",
  },
  {
    name: "Node.js",
    icon: "/images/icons8-nodejs.svg",
  },
  {
    name: "Blynk",
    icon: "/images/Blynk_logo_diamond.png",
  },
];
