const UpdateList = ({ children }: any) => {
  return (
    <div className="mt-12">
      <h2 className="text-xl font-bold">Latest Updates 💡</h2>
      {children}
    </div>
  );
};

export default UpdateList;
